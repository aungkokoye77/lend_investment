<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 10:42
 */

namespace App;

use DateTime;

class Utility
{

    const DATE_FORMAT = 'd/m/Y';

    /**
     * Test date validation by format
     *
     * @param $date
     * @return bool
     */
    static public function isValidDate($date) : bool
    {

        $dateTime = DateTime::createFromFormat(self::DATE_FORMAT, $date);

        return $dateTime && $dateTime->format(self::DATE_FORMAT) === $date;

    }

    /**
     * check positive number or not
     *
     * @param $num
     * @return bool
     */
    static public function isPositiveNumber($num) : bool
    {

        if(is_numeric($num)  && floatval($num) > 0){
            return true;
        }

        return false;

    }

    /**
     * check Not Less than zero
     *
     * @param $num
     * @return bool
     */
    static public function isNotLessThanZero($num) : bool
    {
        if(is_numeric($num)  && floatval($num) >= 0){
            return true;
        }

        return false;
    }

}