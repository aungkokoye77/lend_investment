<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 10:37
 */

namespace App;

use DateTime;

interface Product
{

    public function getStartDate() : DateTime ;
    public function getEndDate() : DateTime;

}