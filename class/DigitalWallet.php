<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 15:16
 */

namespace App;


class DigitalWallet implements Wallet
{

    private $balance;

    /**
     * DigitalWallet constructor.
     * @param $amount
     * @throws \Exception
     */
    public function __construct($amount)
    {
        $this->balanceValidation($amount);
        $this->balance  = $amount;
    }

    /**
     *
     * @throws \Exception
     * @param $amount
     */
    public function setBalance($amount) : void
    {
        $this->balanceValidation($amount);
        $this->balance = $amount;
    }

    /**
     * @return float
     */
    public function getBalance() : float
    {
        return $this->balance;
    }

    /**
     * @param $amount
     * @throws \Exception
     */
    public function balanceValidation($amount) : void
    {
        if(!Utility::isNotLessThanZero($amount)){
            throw new \Exception('Amount is not valid.');
        }
    }
}