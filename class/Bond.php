<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 17:20
 */

namespace App;

use DateTime;

class Bond
{

    const MONTH_YEAR_FORMAT = 'm/Y';
    private $tranche;
    private $investor;
    private $investDateTime;
    private $investAmount;

    /**
     * Bond constructor.
     * @param Tranche  $tranche
     * @param Investor $investor
     * @param          $investDate
     * @param          $investAmount
     * @throws \Exception
     */
    public function __construct(Tranche $tranche, Investor $investor, $investDate, $investAmount)
    {
        $this->investDateValidate($investDate, $tranche);
        $this->investAmountValidate($tranche, $investor, $investAmount);

        $this->tranche        = $tranche;
        $this->investor       = $investor;
        $this->investDateTime = DateTime::createFromFormat(Utility::DATE_FORMAT, $investDate);
        $this->investAmount   = $investAmount;

        $this->tranche->setLimit(
            $this->tranche->getLimit() - $investAmount
        );

        $this->investor->setWalletBalance(
            $this->investor->getWalletBalance() - $investAmount
        );

    }


    /**
     * check invested date is valid date or not .
     * check invested date is within tranche range
     *
     * @param         $investDate
     * @param Tranche $tranche
     * @throws \Exception
     */
    private function investDateValidate($investDate, Tranche $tranche): void
    {
        if (!Utility::isValidDate($investDate)) {
            throw new \Exception('Invest Date is not valid.');
        }

        $investDateTime = \DateTime::createFromFormat(Utility::DATE_FORMAT, $investDate);

        if ($investDateTime < $tranche->getStartDateTime() || $investDateTime > $tranche->getEndDateTime()) {
            throw new \Exception('Invest Date is not within the tranche range.');
        }

    }

    /**
     * check investor has enough fund in wallet.
     * check tranche limit and invested amount match.
     *
     * @param Tranche  $tranche
     * @param Investor $investor
     * @param          $investAmount
     * @throws \Exception
     */
    private function investAmountValidate(Tranche $tranche, Investor $investor, $investAmount): void
    {

        if ($investAmount > $investor->getWalletBalance()) {
            throw new \Exception('Investor has not enough fund.');
        }

        if ($investAmount > $tranche->getLimit()) {
            throw new \Exception('Invested amount is greater than available max tranche limit.');
        }

    }

    /**
     * @param null $triggerDate
     * @return float
     * @throws \Exception
     */
    public function calculateMonthlyInterest($triggerDate = null) :float
    {

        if (empty($triggerDate)) {

            $triggerDateTime = new DateTime();

        } else {

            if (!Utility::isValidDate($triggerDate)) {
                throw new \Exception('Trigger date is not valid.');
            }

            $triggerDateTime = DateTime::createFromFormat(Utility::DATE_FORMAT, $triggerDate);

        }

        $loanCalculatedDateTime = clone $triggerDateTime;
        $loanCalculatedDateTime->modify('-1 month');


        $totalDaysForCalculation = $this->totalDaysForCalculation($loanCalculatedDateTime);

        return $this->calculateInterest($loanCalculatedDateTime, $totalDaysForCalculation);
    }


    /**
     * @param DateTime $loanCalculatedDateTime
     * @return int
     */
    private function totalDaysForCalculation(DateTime $loanCalculatedDateTime) : int
    {

        $calculatedFirstDayDateTime = DateTime::createFromFormat('d/m/Y', $loanCalculatedDateTime->format('1/m/Y'));
        $calculatedLastDayDateTime  = DateTime::createFromFormat('d/m/Y', $loanCalculatedDateTime->format('t/m/Y'));

        $totalDaysInCalculatedMonth = $loanCalculatedDateTime->format('t');
        $loanStarDateTime           = $this->tranche->getStartDateTime();
        $loanEndDateTime            = $this->tranche->getEndDateTime();

        // start date OK / End date OK
        if ($calculatedFirstDayDateTime >= $loanStarDateTime && $calculatedLastDayDateTime <= $loanEndDateTime) {

            //calculated month and invested month is same
            if ($calculatedFirstDayDateTime->format(self::MONTH_YEAR_FORMAT) === $this->investDateTime->format(self::MONTH_YEAR_FORMAT)) {
                return $this->actualTotalDaysForCalculation($this->investDateTime, $calculatedLastDayDateTime);
            } elseif ($calculatedLastDayDateTime < $this->investDateTime) { // invest date is after calculated last date
                return 0;
            } else {
                return $totalDaysInCalculatedMonth;  // invested date is before calculated first date
            }


            //start date not OK / End date not OK
        } elseif ($calculatedFirstDayDateTime > $loanEndDateTime || $calculatedLastDayDateTime < $loanStarDateTime) {

            return 0;

            // start date not OK / End date OK
        } elseif ($calculatedFirstDayDateTime < $loanStarDateTime && $calculatedLastDayDateTime <= $loanEndDateTime) {

            if ($calculatedLastDayDateTime < $this->investDateTime) { // invested date is after calculated last date
                return 0;
            }

            // invested date is before calculated first date
            return $this->actualTotalDaysForCalculation($this->investDateTime, $calculatedLastDayDateTime);

            // start date OK / End date not date OK
        } else {

            //calculated month and invested month is same
            if ($calculatedFirstDayDateTime->format(self::MONTH_YEAR_FORMAT) === $this->investDateTime->format(self::MONTH_YEAR_FORMAT)) {
                return $this->actualTotalDaysForCalculation($this->investDateTime, $loanEndDateTime);
            }

            // invested date is before calculated first date
            return $this->actualTotalDaysForCalculation($calculatedFirstDayDateTime, $loanEndDateTime);

        }

    }


    /**
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     * @return int
     */
    private function actualTotalDaysForCalculation(DateTime $startDateTime, DateTime $endDateTime) : int
    {

        $interval = $endDateTime->diff($startDateTime);
        return $interval->d + 1;

    }

    /**
     * @param DateTime $loanCalculatedDateTime
     * @param          $totalDaysForCalculation
     * @return float
     */
    private function calculateInterest(DateTime $loanCalculatedDateTime, $totalDaysForCalculation) :float
    {

        $monthlyInterest = ($this->tranche->getPercent() * $this->investAmount) / 100;
        $dailyInterest   = $monthlyInterest / $loanCalculatedDateTime->format('t');
        return round($dailyInterest * $totalDaysForCalculation, 2);

    }

}