<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 12:47
 */

namespace App;

use DateTime;

class Tranche
{

    const NAME_LENGTH = 100;

    private $loan;
    private $percent;
    private $limit;
    private $name;


    /**
     * Tranche constructor.
     *
     * @param Product   $loan
     * @param float  $percent
     * @param float  $limit
     * @param string $name
     * @throws \Exception
     */
    public function __construct(Product $loan, $percent, $limit, $name)
    {

        $this->percentValidation($percent);
        $this->limitValidation($limit);
        $this->nameValidation($name);

        $this->loan    = $loan;
        $this->percent = $percent;
        $this->limit   = $limit;
        $this->name    = $name;

    }


    /**
     * @return DateTime
     */
    public function getStartDateTime() : DateTime
    {
        return $this->loan->getStartDate();
    }

    /**
     * @return DateTime
     */
    public function getEndDateTime() : DateTime
    {
        return $this->loan->getEndDate();
    }

    /**
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     *
     * @throws \Exception
     * @param float $percent
     */
    public function setPercent(float $percent): void
    {
        $this->percentValidation($percent);
        $this->percent = $percent;
    }

    /**
     * @return float
     */
    public function getLimit(): float
    {
        return $this->limit;
    }

    /**
     *
     * @throws \Exception
     * @param float $limit
     */
    public function setLimit(float $limit): void
    {
        $this->limitValidation($limit);
        $this->limit = $limit;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @throws \Exception
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->nameValidation($name);
        $this->name = $name;
    }

    /**
     * @param $percent
     * @throws \Exception
     */
    private function  percentValidation($percent) : void
    {
        if (!Utility::isNotLessThanZero($percent)) {
            throw new \Exception('Tranche percent should be positive number or zero.');
        }
    }

    /**
     * @param $limit
     * @throws \Exception
     */
    private function limitValidation($limit) : void
    {

        if (!Utility::isNotLessThanZero($limit)) {

            throw new \Exception('Tranche limit should be positive number or zero.');
        }

    }

    /**
     * @param $name
     * @throws \Exception
     */
    private function nameValidation($name) : void
    {

        if (strlen($name) > self::NAME_LENGTH) {
            throw new \Exception('Tranche name should be less than 100 character.');
        }

    }

}