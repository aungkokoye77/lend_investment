<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 16:14
 */

namespace App;


class Investor
{

    const NAME_LENGTH = 50;
    private $wallet;
    private $name;

    /**
     * Investor constructor.
     * @param Wallet $wallet
     * @param        $name
     * @throws \Exception
     */
    public function __construct(Wallet $wallet, $name)
    {

        $this->nameValidation($name);
        $this->name = $name;
        $this->wallet = $wallet;
    }

    /**
     * @param $name
     * @throws \Exception
     */
    private function nameValidation($name) : void
    {

        if (strlen($name) > self::NAME_LENGTH) {
            throw new \Exception('Tranche name should be less than 50 character.');
        }

    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getWalletBalance() : float
    {
        return $this->wallet->getBalance();
    }

    /**
     * @param $amount
     * @throws \Exception
     */
    public function setWalletBalance($amount) : void
    {
        $this->wallet->setBalance($amount);
    }

}