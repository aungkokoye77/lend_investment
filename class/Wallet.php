<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 15:14
 */

namespace App;


Interface Wallet
{

    public function setBalance($amount);
    public function getBalance();

}