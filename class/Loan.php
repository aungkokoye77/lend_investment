<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 10:39
 */
namespace App;

use DateTime;

class Loan implements Product
{
    private $startDateTime;
    private $endDateTime;

    /**
     * Loan constructor.
     *
     * @param $startDate
     * @param $endDate
     * @throws \Exception
     */
    public function __construct($startDate, $endDate)
    {

        if (!$this->validateDate($startDate, $endDate)) {
            throw new \Exception('Start date or end date is not valid.');
        }

        $startDateTime = DateTime::createFromFormat(Utility::DATE_FORMAT, $startDate);
        $endDateTime   = DateTime::createFromFormat(Utility::DATE_FORMAT, $endDate);

        if (!$this->validateLoanPeriod($startDateTime, $endDateTime)) {
            throw new \Exception('Loan period should be at least one month.');
        }

        $this->startDateTime = $startDateTime;
        $this->endDateTime   = $endDateTime;


    }

    private function validateDate($startDate, $endDate): bool
    {

        if (!Utility::isValidDate($startDate)) {
            return false;
        }

        if (!Utility::isValidDate($endDate)) {
            return false;
        }

        return true;

    }

    private function validateLoanPeriod(DateTime $startDateTime, DateTime $endDateTime): bool
    {

        $expectedEndDateTime = clone $startDateTime;
        $expectedEndDateTime->modify('+1 month');

        return $endDateTime >= $expectedEndDateTime;

    }

    public function getStartDate(): DateTime
    {
        return$this->startDateTime;
    }

    public function getEndDate(): DateTime
    {
        return $this->endDateTime;
    }

}