<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 19/06/19
 * Time: 15:21
 */

require_once './vendor/autoload.php';

use App\DigitalWallet;
use App\Investor;
use App\Loan;
use App\Tranche;
use App\Bond;


$loan = new Loan('01/10/2015', '15/11/2016');

$tranche_A  = new Tranche($loan, 3, 1000, 'A');
$tranche_B  = new Tranche($loan, 6, 1000, 'B');

$wallet_1 = new DigitalWallet(1000);
$wallet_2 = new DigitalWallet(1000);
$wallet_3 = new DigitalWallet(1000);
$wallet_4 = new DigitalWallet(1000);

$investor_1 = new Investor($wallet_1, 'investor_1');
$investor_2 = new Investor($wallet_2, 'investor_2');
$investor_3 = new Investor($wallet_3, 'investor_3');
$investor_4 = new Investor($wallet_4, 'investor_4');


calculateInterest($tranche_A, $investor_1, 1000, '03/10/2015', '01/11/2015');
calculateInterest($tranche_A, $investor_2, 1, '04/10/2015', '01/11/2015');
calculateInterest($tranche_B, $investor_3, 500, '10/10/2015', '01/11/2015');
calculateInterest($tranche_B, $investor_4, 1100, '10/01/2016', '01/11/2015');


/**
 * @param Tranche  $tranche
 * @param Investor $investor
 * @param          $investAmount
 * @param          $investDate
 * @param          $triggerDate
 */
function calculateInterest (Tranche $tranche, Investor $investor, $investAmount, $investDate, $triggerDate)
{
    try{

        $bond = new Bond($tranche, $investor,  $investDate, $investAmount);

        printf("%s got £ %0.2f .<br>",$investor->getName(),$bond->calculateMonthlyInterest($triggerDate) );

    } catch (Exception $e){

        printf("%s cannot calculate interest because %s  <br>", $investor->getName(), $e->getMessage());

    }
}