<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 15:50
 */

namespace Test;

use App\DigitalWallet;
use App\Wallet;
use PHPUnit\Framework\TestCase;

class DigitalWalletTest extends TestCase
{

    /**
     * @dataProvider constructorDataProvider
     *
     * @param $amount
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testConstructor($amount): void
    {

        $wallet            = new DigitalWallet($amount);
        $reflection        = new \ReflectionClass($wallet);
        $reflectionBalance = $this->getPrivateBalance($reflection);

        $this->assertEquals($reflectionBalance->getValue($wallet), $amount);

    }

    /**
     * @param \ReflectionClass $reflection
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getPrivateBalance(\ReflectionClass $reflection): \ReflectionProperty
    {
        $reflectionBalance = $reflection->getProperty('balance');
        $reflectionBalance->setAccessible(true);
        return $reflectionBalance;
    }

    /**
     * @return Wallet
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testSetterBalance() : Wallet
    {
        $wallet            = new DigitalWallet(1000);
        $wallet->setBalance(2000);
        $reflection        = new \ReflectionClass($wallet);
        $reflectionBalance = $this->getPrivateBalance($reflection);
        $this->assertEquals(2000, $reflectionBalance->getValue($wallet));

        return $wallet;
    }

    /**
     * @depends testSetterBalance
     *
     * @param Wallet $wallet
     */
    public function testGetterBalance(Wallet $wallet) : void
    {

        $this->assertEquals(2000, $wallet->getBalance());

    }

    /**
     * @dataProvider exceptionDataProvider
     *
     * @param $amount
     * @throws \Exception
     */
    public function testThrowException($amount): void
    {
        $this->expectException(\Exception::class);
        new DigitalWallet($amount);
    }

    /**
     * @return array
     */
    public function constructorDataProvider(): array
    {
        return [
            [1000],
            [0],
            [0.34],
            [500.34],
            ['0'],
            ['0.33'],
            ['1233'],
            ['22.22']
        ];
    }

    /**
     * @return array
     */
    public function exceptionDataProvider(): array
    {
        return [
            [-1000],
            ['oppp'],
            [-0.34],
            [-500.34],
            ['-0.3'],
            [null]
        ];
    }

}
