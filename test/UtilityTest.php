<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 10:49
 */

namespace Test;

use App\Utility;
use PHPUnit\Framework\TestCase;

class UtilityTest extends TestCase
{

    /**
     * @dataProvider isValidDateDataProvider
     *
     * @param $date
     * @param $expect
     */
    public function testIsValidDate($date, $expect): void
    {

        $this->assertEquals($expect, Utility::isValidDate($date));

    }

    /**
     * @dataProvider isPositiveNumberDataProvider
     *
     * @param $num
     * @param $expect
     */
    public function testIsPositiveNumber($num, $expect): void
    {
        $this->assertEquals($expect, Utility::isPositiveNumber($num));
    }

    /**
     * @dataProvider isLessThanZeroDataProvider
     *
     * @param $num
     * @param $expect
     */
    public function testIsNotLessThanZero($num, $expect) : void
    {

        $this->assertEquals($expect, Utility::isNotLessThanZero($num));

    }


    public function isLessThanZeroDataProvider(): array
    {

        return [
            ['123', true],
            ['23.90', true],
            ['.3', true],
            ['0', true],
            [0, true],
            [0.1, true],
            [1234, true],
            [3500.50, true],
            ['fjgfkj', false],
            ['-4774', false]
        ];

    }

    /**
     * @return array
     */
    public function isValidDateDataProvider(): array
    {

        return [
            ['29/02/2016', true],
            ['28/02/2017', true],
            ['31/12/1999', true],
            ['30/11/2016', true],
            ['29/02/2017', false],
            [null, false],
            ['28-02/2017', false],
            ['3/6/2017', false],

        ];

    }

    public function isPositiveNumberDataProvider()
    {
        return [
            ['123', true],
            ['23.90', true],
            ['.3', true],
            [0.1, true],
            [1234, true],
            [3500.50, true],
            ['fjgfkj', false],
            [0, false],
            ['0', false],
            ['-4774', false]
        ];
    }


}