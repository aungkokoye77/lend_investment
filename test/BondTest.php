<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 25/06/19
 * Time: 14:30
 */

namespace Test;


use App\Bond;
use App\Utility;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BondTest extends TestCase
{

    /**
     * @dataProvider constructorDataProvider
     *
     * @param $loanStartDate
     * @param $loanEndDate
     * @param $tranchePercent
     * @param $trancheLimit
     * @param $trancheName
     * @param $walletAmount
     * @param $investorName
     * @param $investDate
     * @param $investAmount
     * @throws \Exception
     */
    public function testConstructor(
        $loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName,
        $walletAmount, $investorName,
        $investDate, $investAmount
    ): void
    {

        $mockTranche  = $this->getMockTranche($loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName);
        $mockInvestor = $this->getMockInvestor($walletAmount, $investorName);

        $bond = new Bond($mockTranche, $mockInvestor, $investDate, $investAmount);

        $reflectionBond           = new \ReflectionClass($bond);
        $reflectionTranche        = $this->getPrivateProperty($reflectionBond, $bond, 'tranche');
        $reflectionInvestor       = $this->getPrivateProperty($reflectionBond, $bond, 'investor');
        $reflectionInvestDateTime = $this->getPrivateProperty($reflectionBond, $bond, 'investDateTime');
        $reflectionInvestAmount   = $this->getPrivateProperty($reflectionBond, $bond, 'investAmount');


        $this->assertEquals($mockTranche, $reflectionTranche);
        $this->assertEquals($mockInvestor, $reflectionInvestor);
        $this->assertEquals(\DateTime::createFromFormat(Utility::DATE_FORMAT, $investDate), $reflectionInvestDateTime);
        $this->assertEquals($investAmount, $reflectionInvestAmount);

        $this->assertEquals(
            $trancheLimit - $investAmount,
            $reflectionTranche->getLimit()
        );

        $this->assertEquals(
            $walletAmount - $investAmount,
            $reflectionInvestor->getWalletBalance()
        );

    }

    /**
     * @param $loanStartDate
     * @param $loanEndDate
     * @param $tranchePercent
     * @param $trancheLimit
     * @param $trancheName
     * @return MockObject
     */
    public function getMockTranche($loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName): MockObject
    {

        $mockLoan = $this->getMockBuilder('App\Loan')
                         ->disableOriginalConstructor()
                         ->getMock();

        $mockLoan->expects($this->any())->method('getStartDate')
                 ->willReturn(\DateTime::createFromFormat(Utility::DATE_FORMAT, $loanStartDate));

        $mockLoan->expects($this->any())->method('getEndDate')
                 ->willReturn(\DateTime::createFromFormat(Utility::DATE_FORMAT, $loanEndDate));

        $MockTranche = $this->getMockBuilder('App\Tranche')
                            ->setConstructorArgs([$mockLoan, $tranchePercent, $trancheLimit, $trancheName])
                            ->setMethods(null)
                            ->getMock();

        return $MockTranche;
    }

    /**
     * @param $walletAmount
     * @param $investorName
     * @return MockObject
     */
    public function getMockInvestor($walletAmount, $investorName): MockObject
    {
        $mockWallet = $this->getMockBuilder('App\DigitalWallet')
                           ->setConstructorArgs([$walletAmount])
                           ->setMethods(null)
                           ->getMock();

        $mockInvestor = $this->getMockBuilder('App\Investor')
                             ->setConstructorArgs([$mockWallet, $investorName])
                             ->setMethods(null)
                             ->getMock();

        return $mockInvestor;
    }

    /**
     * @param \ReflectionClass $reflectionBond
     * @param Bond             $bond
     * @param                  $name
     * @return mixed
     * @throws \ReflectionException
     */
    public function getPrivateProperty(\ReflectionClass $reflectionBond, Bond $bond, $name)
    {
        $reflectionProperty = $reflectionBond->getProperty($name);
        $reflectionProperty->setAccessible(true);
        return $reflectionProperty->getValue($bond);;
    }

    /**
     * @dataProvider constructorThrowExceptionDataProvider
     *
     * @param $loanStartDate
     * @param $loanEndDate
     * @param $tranchePercent
     * @param $trancheLimit
     * @param $trancheName
     * @param $walletAmount
     * @param $investorName
     * @param $investDate
     * @param $investAmount
     * @throws \Exception
     */
    public function testThrowException(
        $loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName,
        $walletAmount, $investorName,
        $investDate, $investAmount
    ): void
    {
        $this->expectException(\Exception::class);
        $mockTranche  = $this->getMockTranche($loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName);
        $mockInvestor = $this->getMockInvestor($walletAmount, $investorName);
        new Bond($mockTranche, $mockInvestor, $investDate, $investAmount);
    }

    /**
     * @return array
     */
    public function constructorDataProvider(): array
    {
        return [
            [
                '01/10/2015', '15/11/2015', '3', '1000', 'A',
                '1000', 'investor_1',
                '03/10/2015', 1000
            ]
        ];
    }

    /**
     * @return array
     */
    public function constructorThrowExceptionDataProvider(): array
    {
        return [
            [
                '01/10/2015', '15/11/2015', '3', '1000', 'A',
                '1000', 'investor_1',
                '29/02/2015', 1000
            ],
            [
                '01/10/2015', '15/11/2015', '3', '1000', 'A',
                '1000', 'investor_1',
                '21-02/2015', 1000
            ],
            [
                '01/10/2015', '15/11/2015', '3', '1000', 'B',
                '1000', 'investor_2',
                '28/02/2015', 1000
            ],
            [
                '01/10/2015', '15/11/2015', '3', '1000', 'C',
                '1000', 'investor_3',
                '15/10/2015', 1500
            ],
            [
                '01/10/2015', '15/11/2015', '3', '1000', 'D',
                '2000', 'investor_4',
                '15/10/2015', 1500
            ],
        ];
    }

    /**
     * @dataProvider interestCalculationDataProvider
     *
     * @param $loanStartDate
     * @param $loanEndDate
     * @param $tranchePercent
     * @param $trancheLimit
     * @param $trancheName
     * @param $walletAmount
     * @param $investorName
     * @param $investDate
     * @param $investAmount
     * @param $triggerDate
     * @param $expectInterestAmount
     * @throws \Exception
     */
    public function testInterestCalculation(
        $loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName,
        $walletAmount, $investorName,
        $investDate, $investAmount,
        $triggerDate, $expectInterestAmount
    ): void
    {
        $mockTranche  = $this->getMockTranche($loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName);
        $mockInvestor = $this->getMockInvestor($walletAmount, $investorName);

        $bond = new Bond($mockTranche, $mockInvestor, $investDate, $investAmount);

        $this->assertEquals($expectInterestAmount, $bond->calculateMonthlyInterest($triggerDate));

    }

    /**
     * @dataProvider interestCalculationExceptionDataProvider
     *
     * @param $loanStartDate
     * @param $loanEndDate
     * @param $tranchePercent
     * @param $trancheLimit
     * @param $trancheName
     * @param $walletAmount
     * @param $investorName
     * @param $investDate
     * @param $investAmount
     * @param $triggerDate
     * @throws \Exception
     */
    public function testThrowExceptionInterestCalculation(
        $loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName,
        $walletAmount, $investorName,
        $investDate, $investAmount,
        $triggerDate
    ): void
    {
        $this->expectException(\Exception::class);
        $mockTranche  = $this->getMockTranche($loanStartDate, $loanEndDate, $tranchePercent, $trancheLimit, $trancheName);
        $mockInvestor = $this->getMockInvestor($walletAmount, $investorName);

        $bond = new Bond($mockTranche, $mockInvestor, $investDate, $investAmount);
        $bond->calculateMonthlyInterest($triggerDate);

    }

    /**
     * @return array
     */
    public function interestCalculationDataProvider(): array
    {
        return [

            [
                '01/10/2015', '15/11/2015', '3', '1000', 'A',
                '1000', 'investor_1',
                '03/10/2015', 1000,
                '01/11/2015', 28.06
            ],
            [
                '01/10/2015', '15/11/2015', 6, '1000', 'B',
                '1000', 'investor_2',
                '10/10/2015', 500,
                '01/11/2015', 21.29
            ],
            [
                '01/08/2015', '15/12/2015', 6, '1000', 'C',
                '1000', 'investor_3',
                '15/10/2015', 1000,
                '01/10/2015', 0
            ],
            [
                '01/08/2015', '15/12/2015', 6, '1000', 'D',
                '1000', 'investor_4',
                '15/08/2015', 1000,
                '01/09/2015', 32.90
            ],
            [
                '01/08/2015', '15/12/2015', 6, '1000', 'D',
                '1000', 'investor_4',
                '01/08/2015', 1000,
                '01/09/2015', 60
            ],
            [
                '01/08/2015', '15/12/2015', 6, '1000', 'D',
                '1000', 'investor_4',
                '01/09/2015', 1000,
                '01/11/2015', 60
            ],
            [
                '01/08/2015', '15/12/2015', 6, '1000', 'D',
                '1000', 'investor_4',
                '01/08/2015', 1000,
                '01/01/2016', 29.03
            ],
            [
                '01/08/2015', '15/12/2015', 6, '1000', 'D',
                '1000', 'investor_4',
                '05/12/2015', 1000,
                '01/01/2016', 21.29
            ],
            [
                '01/08/2015', '15/12/2015', 6, '1000', 'D',
                '1000', 'investor_4',
                '05/12/2015', 1000,
                null, 0
            ],
            [
                '15/08/2015', '15/12/2015', 3, '1000', 'D',
                '1000', 'investor_4',
                '25/08/2015', 1000,
                '01/09/2015', 6.77
            ],

        ];

    }

    /**
     * @return array
     */
    public function interestCalculationExceptionDataProvider(): array
    {
        return [

            [
                '01/10/2015', '15/11/2015', '3', '1000', 'A',
                '1000', 'investor_1',
                '03/10/2015', 1000,
                '29/02/2015'
            ],
        ];

    }

}