<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 16:23
 */

namespace Test;


use App\Investor;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class InvestorTest extends TestCase
{

    /**
     * @throws \ReflectionException
     * @throws \Exception
     * @return Investor $investor
     */
    public function testConstructor() : Investor
    {

        $mockWallet = $this->getMockWallet(1000);
        $investor = new Investor($mockWallet, 'AA');

        $reflection = new \ReflectionClass($investor);

        $reflectionName = $this->getPrivateName($reflection);
        $reflectionWallet = $this->getPrivateWallet($reflection);

        $this->assertEquals('AA', $reflectionName->getValue($investor));
        $this->assertEquals($mockWallet, $reflectionWallet->getValue($investor));

        return $investor;
    }

    /**
     * @depends testConstructor
     *
     * @param Investor $investor
     * @return Investor
     * @throws \ReflectionException
     */
    public function testSetName(Investor $investor) : Investor
    {
        $investor->setName('BB');
        $reflection = new \ReflectionClass($investor);
        $reflectionName = $this->getPrivateName($reflection);
        $this->assertEquals('BB', $reflectionName->getValue($investor));

        return $investor;
    }

    /**
     * @depends testSetName
     *
     * @param Investor $investor
     * @return Investor
     */
    public function testGetName(Investor $investor) : Investor
    {
        $this->assertEquals('BB', $investor->getName());

        return $investor;
    }

    /**
     * @depends testGetName
     *
     * @param Investor $investor
     * @return Investor
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testSetWallet(Investor $investor) : Investor
    {

        $investor->setWalletBalance(2000);
        $reflectionWallet = $this->getPrivateWallet($reflection = new \ReflectionClass($investor));
        $wallet = $reflectionWallet->getValue($investor);

        $this->assertEquals(2000, $wallet->getBalance());

        return $investor;

    }

    /**
     * @throws \Exception
     */
    public function testThrowException() : void
    {
        $this->expectException(\Exception::class);
        $mockWallet = $this->getMockWallet(1000);
        new Investor(
            $mockWallet,
            'AAmmmmmmmllllllllllmmmmmmmmmmmmmmmmmmmmm memrljrkljwerjwepjr jerjhowierjh ewrjhewjrew'
        );
    }

    /**
     * @depends testSetWallet
     *
     * @param Investor $investor
     * @return Investor
     */
    public function testGetWallet(Investor $investor) : Investor
    {
        $this->assertEquals(2000, $investor->getWalletBalance());

        return $investor;
    }

    /**
     * @param $amount
     * @return MockObject
     */
    public function getMockWallet($amount) : MockObject
    {
        return $this->getMockBuilder('App\DigitalWallet')
             ->setConstructorArgs([$amount])
             ->setMethods(null)
             ->getMock();
    }

    /**
     * @param \ReflectionClass $reflection
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getPrivateName(\ReflectionClass $reflection) : \ReflectionProperty
    {
        $reflectionName = $reflection->getProperty('name');
        $reflectionName->setAccessible(true);
        return $reflectionName;
    }

    /**
     * @param \ReflectionClass $reflection
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getPrivateWallet(\ReflectionClass $reflection) : \ReflectionProperty
    {
        $reflectionWallet = $reflection->getProperty('wallet');
        $reflectionWallet->setAccessible(true);
        return $reflectionWallet;
    }


}