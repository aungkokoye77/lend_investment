<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 11:40
 */

namespace Test;


use App\Loan;
use App\Utility;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{

    /**
     * @dataProvider constructorDataProvider
     *
     * @param $startDate
     * @param $endDate
     * @throws \Exception
     *
     */
    public function testConstructor($startDate, $endDate): void
    {

        $loan = new Loan($startDate, $endDate);

        $reflectionLoan          = new \ReflectionClass($loan);
        $reflectionStartDateTime = $this->getReflectionStartDateTime($reflectionLoan);
        $reflectionEndDateTime   = $this->getReflectionEndDateTime($reflectionLoan);

        $this->assertEquals(
            \DateTime::createFromFormat(Utility::DATE_FORMAT, $startDate),
            $reflectionStartDateTime->getValue($loan)
        );

        $this->assertEquals(
            \DateTime::createFromFormat(Utility::DATE_FORMAT, $endDate),
            $reflectionEndDateTime->getValue($loan)
        );

    }

    /**
     * @@dataProvider exceptionDataProvider
     *
     * @param $startDate
     * @param $endDate
     * @throws \Exception
     */
    public function testThrowException($startDate, $endDate) : void
    {

        $this->expectException(\Exception::class);
        new Loan($startDate, $endDate);

    }

    /**
     * @param \ReflectionClass $reflectionLoan
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getReflectionStartDateTime(\ReflectionClass $reflectionLoan): \ReflectionProperty
    {
        $reflectionStartDateTime = $reflectionLoan->getProperty('startDateTime');
        $reflectionStartDateTime->setAccessible(true);
        return $reflectionStartDateTime;
    }

    /**
     * @param \ReflectionClass $reflectionLoan
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getReflectionEndDateTime(\ReflectionClass $reflectionLoan): \ReflectionProperty
    {
        $reflectionEndDateTime = $reflectionLoan->getProperty('endDateTime');
        $reflectionEndDateTime->setAccessible(true);
        return $reflectionEndDateTime;
    }

    /**
     * @depends      testConstructor
     * @dataProvider constructorDataProvider
     *
     * @param $startDate
     * @param $endDate
     * @throws \Exception
     *
     */
    public function testGetStartDate($startDate, $endDate): void
    {

        $loan                    = new Loan($startDate, $endDate);
        $reflectionLoan          = new \ReflectionClass($loan);
        $reflectionStartDateTime = $this->getReflectionStartDateTime($reflectionLoan);

        $this->assertEquals($reflectionStartDateTime->getValue($loan), $loan->getStartDate());

    }

    /**
     * @depends      testConstructor
     * @dataProvider constructorDataProvider
     *
     * @param $startDate
     * @param $endDate
     * @throws \Exception
     *
     */
    public function testGetEndDate($startDate, $endDate): void
    {

        $loan                  = new Loan($startDate, $endDate);
        $reflectionLoan        = new \ReflectionClass($loan);
        $reflectionEndDateTime = $this->getReflectionEndDateTime($reflectionLoan);

        $this->assertEquals($reflectionEndDateTime->getValue($loan), $loan->getEndDate());
    }

    /**
     * @return array
     */
    public function constructorDataProvider(): array
    {
        return [
            ['29/02/2016', '29/03/2016'],
            ['01/03/2016', '15/07/2016'],
            ['28/02/2017', '01/04/2017']
        ];
    }

    /**
     * @return array
     */
    public function exceptionDataProvider(): array
    {
        return [
            ['29/02/2017', '29/03/2016'],
            ['01/03/2016', '15/03/2016'],
            ['28-02/2017', '01/04/2017'],
            ['33/02/2017', '01/04/2017'],
            ['03/02/2017', null]
        ];
    }

}