<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 24/06/19
 * Time: 13:19
 */

namespace Test;

use App\Tranche;
use App\Utility;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{

    /**
     * @dataProvider constructorDataProvider
     *
     * @param $percent
     * @param $limit
     * @param $name
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testConstructor($percent, $limit, $name)
    {

        $mockLoan   = $this->getMockLoan();
        $tranche    = new Tranche($mockLoan, $percent, $limit, $name);
        $reflection = new \ReflectionClass($tranche);

        $this->assertEquals($mockLoan->getStartDate(), $tranche->getStartDateTime());
        $this->assertEquals($mockLoan->getEndDate(), $tranche->getEndDateTime());
        $this->assertEquals($percent, $this->getPrivatePercent($reflection)->getValue($tranche));
        $this->assertEquals($name, $this->getPrivateName($reflection)->getValue($tranche));

    }

    /**
     * create Loan Mock Object for Tranche constructor
     *
     * @return MockObject
     */
    public function getMockLoan(): MockObject
    {
        $mockLoan = $this->getMockBuilder('App\Loan')
                         ->disableOriginalConstructor()
                         ->setMethods(['getStartDate', 'getEndDate'])
                         ->getMock();

        $mockLoan->expects($this->any())
                 ->method('getStartDate')
                 ->willReturn(\DateTime::createFromFormat(Utility::DATE_FORMAT, '01/10/2015'));

        $mockLoan->expects($this->any())
                 ->method('getEndDate')
                 ->willReturn(\DateTime::createFromFormat(Utility::DATE_FORMAT, '15/12/2015'));

        return $mockLoan;

    }

    /**
     * @param \ReflectionClass $reflection
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getPrivatePercent(\ReflectionClass $reflection): \ReflectionProperty
    {
        $reflectionPercent = $reflection->getProperty('percent');
        $reflectionPercent->setAccessible(true);
        return $reflectionPercent;
    }

    /**
     * @param \ReflectionClass $reflection
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getPrivateName(\ReflectionClass $reflection): \ReflectionProperty
    {
        $reflectionName = $reflection->getProperty('name');
        $reflectionName->setAccessible(true);
        return $reflectionName;
    }

    /**
     * @dataProvider exceptionDataProvider
     *
     * @param $percent
     * @param $limit
     * @param $name
     * @throws \Exception
     */
    public function testException($percent, $limit, $name): void
    {
        $this->expectException(\Exception::class);
        $mockLoan = $this->getMockLoan();
        new Tranche($mockLoan, $percent, $limit, $name);
    }

    /**
     * @return Tranche
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testSetterPercent() : Tranche
    {

        $mockLoan = $this->getMockLoan();
        $tranche  = new Tranche($mockLoan, 3, 100, 'A');
        $tranche->setPercent(6);
        $reflection = new \ReflectionClass($tranche);
        $this->assertEquals(
            $this->getPrivatePercent($reflection)->getValue($tranche),
            6
        );

        return $tranche;

    }


    /**
     * @depends testSetterPercent
     *
     * @param Tranche $tranche
     * @return Tranche
     */
    public function testGetterPercent(Tranche $tranche) : Tranche
    {

        $this->assertEquals(
            6,
            $tranche->getPercent()
        );

        return $tranche;

    }

    /**
     * @depends testGetterPercent
     * @param Tranche $tranche
     * @return Tranche
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testSetterLimit(Tranche $tranche) : Tranche
    {

        $tranche->setLimit(500);
        $reflection = new \ReflectionClass($tranche);

        $this->assertEquals($this->getPrivateLimit($reflection)->getValue($tranche), 500);

        return $tranche;

    }

    /**
     * @depends testSetterLimit
     *
     * @param Tranche $tranche
     * @return Tranche
     */
    public function testGetterLimit(Tranche $tranche) : Tranche
    {
        $this->assertEquals(500, $tranche->getLimit());

        return$tranche;
    }

    /**
     * @depends testGetterLimit
     *
     * @param Tranche $tranche
     * @return Tranche
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function testSetterName(Tranche $tranche) : Tranche
    {
        $tranche->setName('B');

        $reflection = new \ReflectionClass($tranche);

        $this->assertEquals($this->getPrivateName($reflection)->getValue($tranche), 'B');

        return $tranche;

    }

    /**
     * @depends testSetterName
     *
     * @param Tranche $tranche
     */
    public function testGetterName(Tranche $tranche) : void
    {
        $this->assertEquals('B', $tranche->getName());
    }

    /**
     * @param \ReflectionClass $reflection
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getPrivateLimit(\ReflectionClass $reflection): \ReflectionProperty
    {
        $reflectionLimit = $reflection->getProperty('limit');
        $reflectionLimit->setAccessible(true);
        return $reflectionLimit;
    }


    /**
     * @return array
     */
    public function constructorDataProvider(): array
    {

        return [
            [3, 1000, 'A'],
            ['6', 1500.50, 'B'],
            ['0.5', 1500, 'C'],
            ['0.5', 0, 'D'],
            ['0', 1500, 'E'],
        ];

    }

    /**
     * @return array
     */
    public function exceptionDataProvider(): array
    {
        return [
            [-3, 1000, 'A'],
            ['6', -1500.50, 'B'],
            ['0.5', -1, 'C'],
            ['0.5', '-1500', 'D'],
            ['-0.1', 1500, 'E'],
            [
                '0.5',
                300,
                'Cdhgfhsdgjhsdgjfgsjhdgjhsdgfugjhgfsdgfsdgjhghugsdhughjhghsghsgsjgghsgskjghjsghjsghkjshgkjsghgkjh
                skjghhkgjhgkjhsfkjghskfjghkjsfh dfggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg
                ggggggggggggggggggggggggggggggggggggas'
            ]
        ];
    }


}